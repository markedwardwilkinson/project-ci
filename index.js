export default class DataLayer {
    get dataLayerInitialised() {
        return typeof window.dataLayer !== 'undefined';
    }
    get dataLayerDomEventsBound() {
        return !!window.sessionStorage.getItem('datalayer-dom-events-ready');
    }
}
